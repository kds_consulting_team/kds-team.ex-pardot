# Pardot Extractor #

## Overview ##
Pardot is a B2B Marketing Automation platform offering solutions to organize/gather marketing data. The purpose of Pardot Extractor is to allow users to retrieve the marketing related data resources out of Pardot.

## Pardot API Documentations ##
[Pardot API](http://developer.pardot.com/)

## Authentication ##
Required Information

1. Email    
2. Password 
3. UserKey 
    - Available in Pardot under ``{your email address}`` > Settings in the API User Key row

***Note: For user accounts that have `Salesforce User Sync` enabled, user must authenticate with a  `Pardot-only` user.***

## Rate Limits ##
1. Daily Requests
    - Pardot Pro useres are allocated 25,000 API requests per day
    - Pardot Ultimate users can make up to 100,000 API requests per day
2. Concurrent Requests
    - Can have up to 5 concurrent API requests

## Configuration ##
1. Email
    - User login to Pardot platform
2. Password
    - Password used to login to Pardot platform
3. UserKey
    - To acquire this, instruction is described in the `Authentication` Section
4. Endpoints
    - List of endpoints supported for this extractor
5. Backfill mode
    - If disabled, the component will run the date range to last 7 days by default
    - Please ensure to input start date and end date when backfill mode is enabled
    - Please avoid running backfill mode on multiple endpoints at the same time as there will be a possibility that it will flood the component's memory capacity
6. Visitor IDs
    - This configuration is required when `Visits` endpoint is selected
    - Note: The component will only accept `visitor_id`, NOT `visit_id`
7. Email IDs
    - This configuration is required when `Emails` endpoint is selected

## Python Wrapper Used ##
[PyPardot4](https://github.com/mneedham91/PyPardot4)