'''
Template Component main class.

'''

import logging
import logging_gelf.handlers
import logging_gelf.formatters  # noqa
import sys
import os  # noqa
import datetime  # noqa
import dateparser
import pandas as pd
import json

from kbc.env_handler import KBCEnvHandler
from kbc.result import KBCTableDef  # noqa
from kbc.result import ResultWriter  # noqa
from pypardot.client import PardotAPI


# configuration variables
KEY_EMAIL = 'email'
KEY_PASSWORD = '#password'
KEY_USERKEY = '#userkey'

MANDATORY_PARS = [
    KEY_EMAIL,
    KEY_PASSWORD,
    KEY_USERKEY
]
MANDATORY_IMAGE_PARS = []

# Default Table Output Destination
DEFAULT_TABLE_SOURCE = "/data/in/tables/"
DEFAULT_TABLE_DESTINATION = "/data/out/tables/"
DEFAULT_FILE_DESTINATION = "/data/out/files/"
DEFAULT_FILE_SOURCE = "/data/in/files/"

# Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)-8s : [line:%(lineno)3s] %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

if 'KBC_LOGGER_ADDR' in os.environ and 'KBC_LOGGER_PORT' in os.environ:

    logger = logging.getLogger()
    logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
        host=os.getenv('KBC_LOGGER_ADDR'), port=int(os.getenv('KBC_LOGGER_PORT')))
    logging_gelf_handler.setFormatter(
        logging_gelf.formatters.GELFFormatter(null_character=True))
    logger.addHandler(logging_gelf_handler)

    # remove default logging to stdout
    logger.removeHandler(logger.handlers[0])

APP_VERSION = '0.0.7'

# Loading mapping table for special parsing
with open('src/mapping.json', 'r') as f:
    mapping = json.load(f)


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config()
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.error(e)
            exit(1)

    # backfill_mode, start_date, end_date):
    def config_date_range(self, backfill_obj):
        '''
        Setting up request date range
        '''

        # if backfill == 'enable':
        if backfill_obj['backfill'] == 'enable':
            logging.info('Backfill mode: Enabled')
            # self.start_date = params['backfill_mode']['start_date']
            # self.end_date = params['backfill_mode']['end_date']
            self.start_date = backfill_obj['start_date']
            self.end_date = backfill_obj['end_date']
            logging.info('Backfill Start Date: {}'.format(self.start_date))
            logging.info('Backfill End Date: {}'.format(self.end_date))
            # Error control in case user did not input date
            if self.start_date == '' or self.end_date == '':
                logging.error(
                    'Start date or end date cannot be empty with backfill mode enabled.' +
                    'Please enter start date and end date.'
                )
                sys.exit(1)

            # Validate if the input date range is valid
            start_date_temp = dateparser.parse(self.start_date)
            end_date_temp = dateparser.parse(self.end_date)
            if (end_date_temp-start_date_temp).days < 0:
                logging.error('Start date cannot be later than end date.')
                sys.exit(1)
        else:
            self.start_date = 'last_7_days'
            self.end_date = 'today'

    def produce_manifest(self, file_name, primary_key):
        '''
        function for producting manifest
        '''

        file = DEFAULT_TABLE_DESTINATION+'{}.manifest'.format(file_name)
        manifest = {
            "incremental": True,
            "primary_key": primary_key
        }

        try:
            with open(file, 'w') as file_out:
                json.dump(manifest, file_out)
                logging.info("Output manifest file produced.")
        except Exception as e:
            logging.error("Could not produce output file manifest.")
            logging.error(e)

    def special_parsing(self, tablename, data_in):
        '''
        Special treatment for some table
        '''

        data_out = []

        if tablename in mapping:
            table_mapping = mapping[tablename]
            for row in data_in:
                temp_data = {}
                for col in table_mapping:
                    col_delim = col.split('.')
                    col_value = row
                    try:
                        for delim in col_delim:
                            col_value = col_value[delim]
                    except Exception:
                        col_value = ''

                    temp_data[table_mapping[col]['col_name']] = col_value
                data_out.append(temp_data)

        else:
            data_out = data_in

        return data_out

    def output_file(self, data, file_name, column_headers):
        '''
        Output dataframe intput to destination file
        Appending to file if file exist
        '''

        if not os.path.isfile(file_name):
            with open(file_name, 'a') as b:
                data.to_csv(b, index=False, columns=column_headers)
            b.close()
        else:
            with open(file_name, 'a') as b:
                data.to_csv(b, index=False, header=False,
                            columns=column_headers)
            b.close()

        return

    def execute_pardot(self, pardot, endpoint):
        '''
        Calling defined pardot functions
        '''

        # data_out = []
        empty = False
        count = 200
        offset = 0
        column_headers = []
        # Parameters to check if last value of the array is same as last itr
        same_last_value = False
        last_array_value = ''
        bulk_bool = False
        # Importing retry method
        retry_bool = True
        retry_n = 0

        while count == 200 and not same_last_value and retry_bool:
            try:
                if endpoint == 'campaigns':
                    data_in = pardot.campaigns.query(
                        # output='bulk',
                        format='json',
                        # created_after=self.start_date, created_before=self.end_date,
                        offset=offset)
                    data_temp = data_in['campaign']
                elif endpoint == 'customfields':
                    data_in = pardot.customfields.query(
                        # output='bulk',
                        format='json',
                        # created_after=self.start_date, created_before=self.end_date,
                        offset=offset)
                    data_temp = data_in['customField']
                elif endpoint == 'customredirects':
                    data_in = pardot.customredirects.query(
                        # output='bulk',
                        format='json',
                        # created_after=self.start_date, created_before=self.end_date,
                        offset=offset)
                    data_temp = data_in['customRedirect']
                elif endpoint == 'dynamiccontent':
                    data_in = pardot.dynamiccontent.query(
                        # output='bulk',
                        format='json',
                        # created_after=self.start_date, created_before=self.end_date,
                        offset=offset)
                    data_temp = data_in['dynamicContent']
                elif endpoint == 'emailclicks':
                    bulk_bool = True
                    data_in = pardot.emailclicks.query(
                        output='bulk',
                        format='json',
                        created_after=self.start_date,
                        created_before=self.end_date,
                        # offset=offset)
                        id_greater_than=last_array_value)
                    data_temp = data_in['emailClick']

                elif endpoint == 'forms':
                    data_in = pardot.forms.query(
                        # output='bulk',
                        format='json',
                        # created_after=self.start_date, created_before=self.end_date,
                        offset=offset)
                    data_temp = data_in['form']
                elif endpoint == 'lifecyclehistories':
                    data_in = pardot.lifecyclehistories.query(
                        # output='bulk',
                        format='json',
                        created_after=self.start_date,
                        created_before=self.end_date,
                        offset=offset)
                    data_temp = data_in['lifecycleHistory']
                elif endpoint == 'lifecyclestages':
                    data_in = pardot.lifecyclestages.query(
                        # output='bulk',
                        format='json',
                        # created_after=self.start_date, created_before=self.end_date,
                        offset=offset)
                    data_temp = data_in['lifecycleStage']
                elif endpoint == 'lists':
                    data_in = pardot.lists.query(
                        # output='bulk',
                        format='json',
                        # created_after=self.start_date, created_before=self.end_date,
                        offset=offset)
                    data_temp = data_in['list']
                elif endpoint == 'listmemberships':
                    data_in = pardot.listmemberships.query(
                        # output='bulk',
                        format='json',
                        created_after=self.start_date, created_before=self.end_date,
                        offset=offset)
                    data_temp = data_in['list_membership']

                elif endpoint == 'opportunities':
                    data_in = pardot.opportunities.query(
                        # output='bulk',
                        format='json',
                        created_after=self.start_date, created_before=self.end_date,
                        offset=offset)
                    data_temp = data_in['opportunity']
                    # data_temp = self.special_parsing(endpoint, data_in['opportunity'])
                elif endpoint == 'prospects':
                    bulk_bool = True
                    data_in = pardot.prospects.query(
                        output='bulk',
                        format='json',
                        created_after=self.start_date,
                        created_before=self.end_date,
                        offset=offset)
                    # id_greater_than=last_array_value)
                    data_temp = data_in['prospect']
                elif endpoint == 'prospectaccounts':
                    data_in = pardot.prospectaccounts.query(
                        # output='bulk',
                        format='json',
                        created_after=self.start_date, created_before=self.end_date,
                        offset=offset)
                    data_temp = data_in['prospectAccount']
                elif endpoint == 'tags':
                    data_in = pardot.tags.query(
                        # output='bulk',
                        format='json',
                        # created_after=self.start_date, created_before=self.end_date,
                        offset=offset)
                    data_temp = data_in['tag']
                elif endpoint == 'tagobjects':
                    data_in = pardot.tagobjects.query(
                        # output='bulk',
                        format='json',
                        created_after=self.start_date, created_before=self.end_date,
                        offset=offset)
                    data_temp = data_in['tagObject']

                elif endpoint == 'users':
                    data_in = pardot.users.query(
                        # output='bulk',
                        format='json',
                        # created_after=self.start_date, created_before=self.end_date,
                        offset=offset)
                    data_temp = data_in['user']
                elif endpoint == 'visitors':
                    bulk_bool = True
                    data_in = pardot.visitors.query(
                        output='bulk',
                        format='json',
                        created_after=self.start_date, created_before=self.end_date,
                        offset=offset)
                    # id_greater_than=last_array_value)
                    data_temp = data_in['visitor']
                elif endpoint == 'visitoractivities':
                    bulk_bool = True
                    data_in = pardot.visitoractivities.query(
                        output='bulk',
                        format='json',
                        created_after=self.start_date,
                        created_before=self.end_date,
                        offset=offset)
                    # id_greater_than=last_array_value)
                    data_temp = data_in['visitor_activity']
                elif endpoint == 'visits':
                    data_in = pardot.visits.query_by_visitor_ids(
                        visitor_ids=self.visit_id)
                    data_temp = data_in['visit']
                elif endpoint == 'emails':
                    data_temp = []
                    for email_id in self.email_id:
                        data_in = pardot.emails.stats(
                            list_email_id=email_id
                        )
                        data_to_insert = data_in['stats']
                        data_to_insert['id'] = email_id
                        data_temp.append(data_to_insert)
                else:
                    logging.error(
                        'Endpoint [{}] does not exist. Please contact support'.format(endpoint))
                    sys.exit(1)

                # Special parsing on endpoints if needed
                data_temp_parsed = self.special_parsing(endpoint, data_temp)
                data_out = pd.DataFrame(data_temp_parsed)

                # Pagination parameters settings
                count = len(data_temp)
                # if last value of the array is same as last itr
                if count == 200:
                    array_last_value = data_temp[199]
                    if bulk_bool:
                        last_array_value = array_last_value['id']
                    else:
                        if array_last_value == last_array_value:
                            same_last_value = True
                        else:
                            last_array_value = array_last_value

                if offset == 0:
                    column_headers = list(data_out.columns)
                offset = offset + 200

                # Adding temp data into the big queue
                # data_out = data_out + data_temp_parsed
                if count == 0:
                    empty = True
                else:
                    self.output_file(data_out, DEFAULT_TABLE_DESTINATION +
                                     '{}.csv'.format(endpoint), column_headers)
                    logging.info(
                        "[{0}] pagination - count:{1}, offset: {2}".format(endpoint, count, offset))

            # except Exception as e:
            #     if offset == 0:
            #         empty = True
            #         count = 0
            #     logging.error(
            #         "There is an issue with the request. Please check error message: {}".format(e))
            #     sys.exit(1)

            # Query retry
            except Exception as e:
                logging.error(
                    "There is an issue with the request. Please check error message: {}".format(e))
                if retry_n < 5:
                    logging.info("Retrying...{}".format(retry_n))
                    retry_n += 1
                else:
                    retry_bool = False
                    sys.exit(1)

        return empty, data_out

    def run(self):
        '''
        Main execution code
        '''

        params = self.cfg_params  # noqa
        # Validating input parameters
        if params == {}:
            logging.error(
                'Please input required parameters: Email, Password, UserKey, Endpoint.')
            sys.exit(1)
        input_email = params['email']
        input_password = params['#password']
        input_userkey = params['#userkey']
        if input_email == '' or input_password == '' or input_userkey == '':
            logging.error('Please enter required information: Email, Password, UserKey'
                          )
            sys.exit(1)

        endpoints_unparsed = params['endpoint']
        # Converting endpoint objects to endpoint list
        endpoints = []
        for i in endpoints_unparsed:
            endpoints.append(i['endpoint'])

        # Validating if the custom ids are entered for special endpoints
        if 'visits' in endpoints:
            self.visit_id = params['visit_id']
            if self.visit_id == '':
                logging.error(
                    '[Visits] endpoint is selected. However, <visitor ids> are missing in the configuration. '
                    'Please enter <visitor ids>.')
                sys.exit(1)
        if 'emails' in endpoints:
            tmp_email_id = params['email_id'].replace(' ', '')
            self.email_id = tmp_email_id.split(',')
            if len(self.email_id) == 0:
                logging.error(
                    '[Emails] endpoint is selected. However, <email ids> are missing in the configuration. '
                    'Please enter <email ids>')
                sys.exit(1)

        # Configuring date range
        self.config_date_range(
            backfill_obj=params['backfill_mode']
        )

        # Pardot Integration
        pardot = PardotAPI(
            email=input_email,
            password=input_password,
            user_key=input_userkey
        )
        pardot.authenticate()

        # Looping through all the input in the configuration
        for endpoint in endpoints:
            logging.info('Extracting [{}]...'.format(endpoint))
            empty, endpoint_data = self.execute_pardot(
                pardot, endpoint)
            if empty:
                logging.info(
                    'Endpoint [{}] contains no data.'.format(endpoint))
            else:
                logging.info('Outputting [{}]'.format(endpoint))
                self.produce_manifest('{}.csv'.format(
                    endpoint), ['id'])

        logging.info("Extraction finished")


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug = sys.argv[1]
    else:
        debug = True
    comp = Component(debug)
    comp.run()
